const path = require('path');

module.exports = {
  mode: 'development',
  entry: './index.js',
  watch: true,
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'build'),
    publicPath: '/',
  },
  resolve: {
    extensions: ['.js', '.json'],
  },
  module: {
    rules: [
      {
        oneOf: [
          {
            test: /\.(js)$/,
            loader: 'babel-loader',
            include: path.resolve(__dirname),
            exclude: /node_modules/,
          },
        ],
      },
    ],
  },
  plugins: [],
  devtool: 'source-map',
  stats: {
    assets: true,
    children: false,
    entrypoints: false,
    chunks: false,
    colors: true,
    performance: false,
    usedExports: false,
    modules: false,
  },
};
