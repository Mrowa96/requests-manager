import {
  RequestsManagerGlobal,
  Request,
  RequestStatus,
  RequestMethod,
} from 'requests-manager';

const buttons = Array.from(document.getElementsByClassName('button'));
const testButton = document.getElementById('test-button');
const requestsQuantityElement = document.getElementById('requests-quantity');

const requestsQuantity = 0;

function updateRequestQuantityElement(value) {
  if (requestsQuantityElement) {
    requestsQuantityElement.innerHTML = value.toString();
  }
}

RequestsManagerGlobal.subscribe({
  notify({ requests }) {
    updateRequestQuantityElement(requests.length);

    const successfulRequestsQuantity = requests.filter(request =>
      [
        RequestStatus.SUCCESS,
        RequestStatus.FAILURE,
        RequestStatus.CANCEL,
      ].includes(request.status),
    ).length;

    if (successfulRequestsQuantity === requests.length && testButton) {
      testButton.removeAttribute('disabled');
    }
  },
});

updateRequestQuantityElement(requestsQuantity);

buttons.forEach(button => {
  button.addEventListener('click', function() {
    button.setAttribute('disabled', 'disabled');

    const cancelButton = document.createElement('button');
    cancelButton.setAttribute('type', 'button');
    cancelButton.innerHTML = 'Cancel';
    button.parentNode.insertBefore(cancelButton, button.nextSibling);

    const request = new Request(
      'https://jsonplaceholder.typicode.com/todos',
      {
        method: RequestMethod.GET,
      },
      Math.floor(Math.random() * 5 + 1) * 1000,
    );
    request.onFew(
      [RequestStatus.FAILURE, RequestStatus.SUCCESS, RequestStatus.CANCEL],
      () => {
        button.removeAttribute('disabled');
        cancelButton.remove();
      },
    );

    cancelButton.addEventListener('click', function() {
      request.cancel();
    });

    RequestsManagerGlobal.addRequest(request);
  });
});

if (testButton) {
  testButton.addEventListener('click', function() {
    if (!testButton.disabled) {
      buttons.forEach(button => {
        button.click();
      });

      testButton.setAttribute('disabled', 'disabled');
    }
  });
}
