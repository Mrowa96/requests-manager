export const RequestStatus = {
  INITIALIZED: 'initialized',
  IN_PROGRESS: 'inProgress',
  SUCCESS: 'success',
  FAILURE: 'failure',
  CANCEL: 'cancel',
};

export const RequestMethod = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
  HEAD: 'HEAD',
  OPTIONS: 'OPTIONS',
};
