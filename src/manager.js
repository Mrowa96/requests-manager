import { RequestStatus } from './constants';

export default function RequestsManager(state = { requests: [] }) {
  this.subscribers = [];
  this.state = state;
}

RequestsManager.prototype.addRequest = function(request, send = true) {
  request.onFew(
    [
      RequestStatus.IN_PROGRESS,
      RequestStatus.SUCCESS,
      RequestStatus.FAILURE,
      RequestStatus.CANCEL,
    ],
    () => {
      this.notifySubscribers();
    },
  );

  this.state.requests = [...this.state.requests, request];

  if (send === true) {
    request.send();
  }

  this.notifySubscribers();
};

RequestsManager.prototype.removeRequest = function(requestId) {
  this.state.requests = this.state.requests.filter(
    item => item.id !== requestId,
  );
  this.notifySubscribers();
};

RequestsManager.prototype.subscribe = function(subscriber) {
  this.subscribers.push(subscriber);
};

RequestsManager.prototype.notifySubscribers = function() {
  this.subscribers.forEach(subscriber => subscriber.notify(this.state));
};

export const RequestsManagerGlobal = (function() {
  let instance = null;

  if (!instance) {
    instance = new RequestsManager();
  }

  return instance;
})();
