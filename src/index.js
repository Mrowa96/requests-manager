import RequestsManager, { RequestsManagerGlobal } from './manager';
import Request from './request';
import { RequestStatus, RequestMethod } from './constants';

export default RequestsManager;

export {
  RequestsManager,
  RequestsManagerGlobal,
  Request,
  RequestStatus,
  RequestMethod,
};
