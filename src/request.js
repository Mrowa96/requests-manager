import uniqid from 'uniqid';
import { RequestStatus, RequestMethod } from './constants';

export default function Request(
  url,
  options = { method: RequestMethod.GET },
  delay = 0,
) {
  this.id = uniqid();
  this.status = RequestStatus.INITIALIZED;
  this.controller = new AbortController();
  this.response = null;
  this.url = url;
  this.options = options;
  this.delay = delay;

  this.events = {
    [RequestStatus.IN_PROGRESS]: [],
    [RequestStatus.SUCCESS]: [],
    [RequestStatus.FAILURE]: [],
    [RequestStatus.CANCEL]: [],
  };
}

Request.prototype.send = function() {
  // TODO private without clone on every dispatch
  function makeRequest() {
    fetch(this.url, { ...this.options, signal: this.controller.signal })
      .then(response => {
        if (!response.ok) {
          this.status = RequestStatus.FAILURE;
          this.trigger(RequestStatus.FAILURE);
        } else {
          this.status = RequestStatus.SUCCESS;
          this.response = response;
          this.trigger(RequestStatus.SUCCESS, this.response);
        }
      })
      .catch(() => {
        this.status = RequestStatus.FAILURE;
        this.trigger(RequestStatus.FAILURE);
      });

    this.status = RequestStatus.IN_PROGRESS;
    this.trigger(RequestStatus.IN_PROGRESS);
  }

  if (this.delay > 0) {
    setTimeout(() => {
      makeRequest.call(this);
    }, this.delay);
  } else {
    makeRequest.call(this);
  }
};

Request.prototype.cancel = function() {
  if (
    ![RequestStatus.IN_PROGRESS, RequestStatus.INITIALIZED].includes(
      this.status,
    )
  ) {
    throw new Error('Cannot cancel non-cancelable request');
  }

  // TODO Add polyfill
  this.controller.abort();
  this.status = RequestStatus.CANCEL;
  this.trigger(RequestStatus.CANCEL);
};

// should be private !!!
// change events to proxy with get and set?
Request.prototype.trigger = function(eventName, data = null) {
  if (!this.supportEvent(eventName)) {
    throw new Error('Event is not supported.');
  }

  this.events[eventName].forEach(callback => callback(data));
};

Request.prototype.on = function(eventName, callback) {
  if (!this.supportEvent(eventName)) {
    throw new Error('Event is not supported.');
  }

  this.events[eventName].push(callback);
};

Request.prototype.onFew = function(eventNames, callback) {
  if (!Array.isArray(eventNames)) {
    throw new Error('"eventNames" should be an array');
  }

  eventNames.forEach(eventName => {
    if (!this.supportEvent(eventName)) {
      throw new Error('Event is not supported.');
    }

    this.events[eventName].push(callback);
  });
};

Request.prototype.supportEvent = function(eventName) {
  return !!this.events[eventName] && Array.isArray(this.events[eventName]);
};
