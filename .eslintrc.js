module.exports = {
  extends: [
    'airbnb-base',
    'plugin:prettier/recommended',
    'prettier',
    'prettier/standard',
  ],
  plugins: ['prettier', 'jest'],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 9,
  },
  rules: {
    'consistent-return': 'off',
    'no-plusplus': 'off',
    'func-names': 'off',
    'import/no-unresolved': 'off',
    'import/no-extraneous-dependencies': 'off',
  },
  env: {
    browser: true,
    'jest/globals': true,
  },
};
