# Version 0.5.0

Initial release, work in progress

# Version 0.6.0

Rewrite example code (may not work correctly), update package.json

# Version 0.7.0

Finally resolve issues with library building and xample configuration, add .eslintignore
